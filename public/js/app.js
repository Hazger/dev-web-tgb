(function(){
	var CONFIG = {
		"cookieName": "loggedUser",
		"cancelGameMessage": "Tem certeza de que deseja cancelar esta partida?",
		"eraseDrawingMessage": "Tem certeza de que deseja limpar a área de desenho?",
		"cannotBuyItem": "Você não pode comprar este item...",
		"boughtItem": "Item adquirido com sucesso!",
		"drawingSent": "Seu desenho foi enviado com sucesso!",
		"drawingSendError": "Não foi possível enviar o seu desenho, por favor tente novamente mais tarde :(",
		"userAlreadyExists": "O nome de usuário ou e-mail escolhido já foi utilizado. Altere os dados inseridos.",
		"userCreated": "Usuário criado com sucesso! Você será redirecionado para a página de login.",
		"userCreationError": "Ocorreu um erro ao criar o usuário. Tente novamente mais tarde!",
		"userNotFound": "Usuário ou senha incorretos.",
		"userLoginError": "Não foi possível efetuar seu login. Tente novamente mais tarde.",
		"drawingGuessedCorrectly": "Você adivinhou o desenho!"
	}
	var app = angular.module('drawingApp', ['ngCookies']);

	app.directive("navigationBar", function() {
		return {
			restrict: 'E',
			templateUrl: 'navigation-bar.html'
		};
	});


	app.directive("compareTo", function() {
    	return {
        	require: "ngModel",
        	scope: {
            	otherModelValue: "=compareTo"
        	},
        	link: function(scope, element, attributes, ngModel) {

            	ngModel.$validators.compareTo = function(modelValue) {
	                return modelValue == scope.otherModelValue;
    	        };

        	    scope.$watch("otherModelValue", function() {
            	    ngModel.$validate();
            	});
        	}
    	};
	});

	// CONTROLLERS

	/***
		Controller utilizado para simular o login do usuário
	***/
	// O login está sendo SIMULADO no Javascript. Quem
	// realiza o controle da autenticação é o servidor
	app.controller('LoginController', ['$window', '$http', function($window, $http) {
		this.username = '';
		this.password = '';
		this.login = function() {
			$http.post('users/login', {
				username: this.username,
				password: this.password
			}).success(function(data){
				$window.location.href = 'index.html';
	    	}).error(function(data, status) {
	    		if (status == 404) {
	    			alert(CONFIG.userNotFound);
	    		} else {
	    			alert(CONFIG.userLoginError);
	    		}
	    	});
		}
	}]);

	/***
		Controller utilizado para efetuar o logout do usuário
	***/
	app.controller('LogoutController', ['$cookieStore', function($cookieStore) {
		this.logout = function() {
			$cookieStore.remove(CONFIG.cookieName);
		}
	}]);

	/***
		Controller utilizado para simular a criação da conta do usuário
	***/
	// A criação da conta está sendo SIMULADA no Javascript. Quem
	// realiza a criação da conta é o servidor
	app.controller('RegistrationController', ['$window', '$http', function($window, $http) {
		this.username = '';
		this.register = function() {
			$http.post('users/create-user', {
				username: this.username,
				password: this.password,
				email: this.email,
				color: this.color
			}).success(function(data){
				alert(CONFIG.userCreated);
				$window.location.href = 'index.html';
	    	}).error(function(data, status) {
	    		if (status == 400) {
	    			alert(CONFIG.userAlreadyExists);
	    		} else {
	    			alert(CONFIG.userCreationError);
	    		}
	    	});
		}
	}]);

	/***
		Controller utilizado para exibir e esconder as seções da aplicação de acordo com
		o conteúdo que está sendo visualizado pelo usuário
	***/
	app.controller('ScreenController', ['$cookieStore', '$window', function($cookieStore, $window){
		var screen = this;
		screen.currentScreen = '';

		this.setCurrentScreen = function(newCurrentScreen) {
			screen.currentScreen = newCurrentScreen;
		}
	}]);

	/***
		Controller utilizado para obter informações do usuário e
		ferramentas adquiridas por ele
	***/
	app.controller('UserController', ['$http', function($http){
		var user = this;
		user.info = [];

		// Carrega as informações sobre o usuário
		$http.get('/users/user').success(function(data){
      		user.info = data;
    	});

    	this.hasItem = function(itemType, itemId) {
    		if (itemType === 'color') {
    			for (var i = 0; i < user.info.availableColors.length; i++) {
    				if (user.info.availableColors[i].id === itemId) {
    					return true;
    				}
    			}
    		} else {
    			for (var i = 0; i < user.info.availableTools.length; i++) {
    				if (user.info.availableTools[i].id === itemId) {
    					return true;
    				}
    			}
    		}
    	}
	}]);

	/***
		Controller de uma nova partida
	***/
	app.controller('DrawingController', ['$window', '$http', function($window, $http) {
		var drawingGame = this;


		// Limpa o canvas
		this.erase = function() {
    		if (confirm(CONFIG.eraseDrawingMessage)) {
    			clearCanvas();
	    		clearDrawingCanvas();
	    	}
		}

		// Envia o desenho para o usuário selecionado anteriormente
		this.send = function() {
			drawingGame.setDrawingUserDrawing($(canvas)[0].toDataURL());
			$http.post('/drawings/send-drawing-to-user', {toUser:drawingGame.drawingModel.toUser, userDrawing:drawingGame.drawingModel.userDrawing, theme:drawingGame.drawingModel.theme}).success(function(data){
				if (data.drawingSent) {
	      			alert(CONFIG.drawingSent);
	      			$window.location.href = 'index.html';
	      		} else {
	      			alert(CONFIG.drawingSendError);
	      		}
	    	});
		}

		// Cancela o jogo em andamento redirecionamento o usuário
		// novamente para a tela inicial da aplicação
		this.cancel = function() {
			if (confirm(CONFIG.cancelGameMessage)) {
				$window.location.href = 'index.html';
			}
		}

		this.searchUser = function(query) {
			/*
				PREVÊ A PESQUISA POR USUÁRIOS
				------
				Comentado neste momento pois o back end não está implementado,
				então a lista será sempre a mesma...
				------
			$http.get('json/users.json?q=' + query).success(function(data){
      			drawingGame.availableUsers = data;
    		});
			*/
		}

		this.clear = function() {
			drawingGame.drawingOptions = [];
			drawingGame.availableUsers = [];
			// Modelo que será enviado para o servidor
			// para o servidor quando o usuário concluir o desenho
			// Propriedades no final do método
			drawingGame.drawingModel = [];

			// Modelo que será enviado para o servidor
			// para o servidor quando o usuário concluir o desenho
			drawingGame.drawingModel.theme = '';
			drawingGame.drawingModel.toUser = '';
			drawingGame.drawingModel.userDrawing = '';

			// Cria o canvas e adiciona os controles para o mouse
			prepareCanvas();

			// Carrega as opções de desenho a partir de uma requisição a um JSON
			$http.get('words/word-options').success(function(data){
	      		drawingGame.drawingOptions = data;
	    	});

			// Carrega a lista de usuários a partir de uma requisição a um JSON
			// Assume-se que este JSON trará apenas 10 usuários. Para carregar outros
			// usuários o jogador terá de digitar algum termo de pesquisa
	    	$http.get('/users/').success(function(data){
	      		drawingGame.availableUsers = data;
	    	});
		}

		// Setters para o modelo
		this.setDrawingTheme = function(drawingTheme) {
			drawingGame.drawingModel.theme = drawingTheme;
		}

		this.setDrawingToUser = function(toUser) {
			drawingGame.drawingModel.toUser = toUser;
		}

		this.setDrawingUserDrawing = function(userDrawing) {
			drawingGame.drawingModel.userDrawing = userDrawing;
		}

		this.pickColor = function(newColor) {
			corAtual = newColor.details;
		}

		this.pickTool = function(newTool) {
			tamanhoAtual = newTool.details;
		}

		drawingGame.clear();
	}]);


	app.controller('GuessingController', ['$http', '$window', function($http, $window){
		var guessingGame = this;
		// Valor do campo de pesquisa por usuários que enviaram desenhos
		guessingGame.searchDrawingUserQuery = '';
		guessingGame.drawingGuess = '';

    	this.searchUser = function(query, currentUser) {
			/*
				PREVÊ A PESQUISA DOS DESENHOS
				------
				Comentado neste momento pois o back end não está implementado,
				então a lista será sempre a mesma...
				------
			$http.get('json/drawing-users.json?q=' + query + '&toUser=' + currentUser).success(function(data){
      			guessingGame.drawingToGuess = data;
    		});
			*/
		}

		this.getColor = function(numberOfPoints) {
			color = 'level-easy';
			if (numberOfPoints === 3) {
				color = 'level-medium';
			}
			if (numberOfPoints === 7) {
				color = 'level-hard';
			}
			return color;
		}

		this.setDrawingToGuess = function(drawingToGuess) {
			guessingGame.drawingGuess = '';
			guessingGame.triedToGuess = false;
			guessingGame.selectedDrawingToGuessCorrectlyGuessed = false;

			var img = new Image();
		    img.onload = function() {
		    	$("#drawing-to-guess").attr("width", canvasWidth);
				$("#drawing-to-guess").attr("height", canvasHeight);
		    	$("#drawing-to-guess")[0].getContext("2d").drawImage(img, 0, 0);
    		};
    		img.src = drawingToGuess.drawing;

			guessingGame.selectedDrawingToGuess = drawingToGuess;
			guessingGame.searchDrawingUserQuery = '';
		}

		this.guess = function(drawingToGuess, guess, userInfo) {
			guessingGame.checkingGuess = true;
			/*
				Verifica se a palavra foi adivinhada corretamente
			*/
			$http.get('drawings/guess?id=' + drawingToGuess._id + '&guess=' + guess)
			.success(function(data){
				guessingGame.triedToGuess = true;

				if (data.guessed) {
      				alert(CONFIG.drawingGuessedCorrectly);
      				$window.location.href = 'index.html';
      			} else {
      				guessingGame.selectedDrawingToGuessCorrectlyGuessed = false;
      			}
      			guessingGame.checkingGuess = false;
    		})
    		.error(function(){
    			guessingGame.triedToGuess = true;
    			guessingGame.checkingGuess = false;
    			guessingGame.checkingGuessError = true;
    		});
		}

		guessingGame.clear = function() {
			guessingGame.drawingToGuess = [];
			guessingGame.selectedDrawingToGuess = {};
			guessingGame.checkingGuess = false;
			guessingGame.checkingGuessError = false;
			guessingGame.triedToGuess = false;
			guessingGame.selectedDrawingToGuessCorrectlyGuessed = false;

			// Carrega as informações sobre o usuário
			$http.get('drawings/drawings-to-guess').success(function(data){
	      		guessingGame.drawingToGuess = data;
	    	});
		}

		guessingGame.clear();
	}]);

	/***
		Controller da loja
	***/
	app.controller('ShoppingController', ['$window', '$http', function($window, $http) {
		var shopping = this;
		shopping.availableItems = [];

		shopping.clear = function() {
			// Carrega os itens para aquisição
			$http.get('items/tools').success(function(data){
	      		shopping.availableItems = data;
	    	});
		}

		shopping.buyItem = function(itemToBuy, userInfo, itemType) {
			// Faz a requisição para o servidor verificando se o usuário pode comprar o item
			$http.get('items/tools?id=' + itemToBuy.id + '&type=' + itemType).success(function(data){
				var couldBuyItem = data.couldBuyItem;

				// ADICIONA ALGUMAS OPERAÇÕES ESTÁTICAS
				// Estas operações não seriam adicionadas se o lado
				// do servidor estivesse implementado
				if (userInfo.points >= itemToBuy.price) {
					userInfo.points -= itemToBuy.price;
					couldBuyItem = true;
				} else {
					couldBuyItem = false;
				}
				// Fim das validações estáticas

	      		if (couldBuyItem) {
		      		if (itemType === 'color') {
						userInfo.availableColors.push(itemToBuy);
					} else {
						userInfo.availableTools.push(itemToBuy);
					}
					$http.post('/users/atualizacompra', {'points':userInfo.points, 'availableTools':userInfo.availableTools, 'availableColors':userInfo.availableColors, 'username': userInfo.username}).success(function(data){
						alert(CONFIG.boughtItem);
					});
				} else {
					alert(CONFIG.cannotBuyItem);
				}
	    	});
		}

		shopping.clear();
	}]);
})();
