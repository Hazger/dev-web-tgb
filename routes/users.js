var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var router = express.Router();
router.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));
router.use(cookieParser());

var User = require('../models/user').User;

// Verifica dados do usuario
router.post('/login', function(req, res) {
	paramUsername = req.body.username;
	paramPassword = req.body.password;

	User.find({'username': paramUsername, 'password': paramPassword}, function(err, docs) {
	    if(!err) {
	    	if (docs.length > 0) {
	    		res.cookie('loggedUser', paramUsername, { maxAge: 900000 });
	      		res.json(200, { message: 'Usuario correto!' });
	  		} else {
	  			res.json(404, { error: 'Nenhum usuario encontrado' });
	  		}
	    } else {
	     	res.json(500, { error: 'Nao foi possivel verificar o usuario' });
	    }
	});
});

// Lista todos os usuarios
router.get('/', function(req, res) {
	if (req.cookies && req.cookies.loggedUser) {
		User.find({"username" : {"$not" : new RegExp(req.cookies.loggedUser)}}, {'username': 1, 'points': 1, 'email': 1, 'color': 1, '_id': 0}, function(err, docs) {
		    if(!err) {
		      res.json(200, { users: docs });
		    } else {
		      res.json(500, { message: err });
		    }
		});
	} else {
		res.json(400, { message: 'Requisicao nao autorizada' });
	}
});

// Lista as informações do usuário
router.get('/user', function(req, res) {
	if (req.cookies && req.cookies.loggedUser) {
		User.find({'username' : req.cookies.loggedUser}, {'_id': 0, 'password': 0, '__v': 0}, function(err, docs) {
		    if(!err) {
		      res.json(200, docs[0]);
		    } else {
		      res.json(500, { message: err });
		    }
		});
	} else {
		res.json(400, { message: 'Requisicao nao autorizada' });
	}
});

// Usuario compra item
router.post('/atualizacompra' , function(req, res){
  User.findOne({"username" : req.body.username}, function(err, usuario){
    console.log(req.body.username);
    if (!err) {
      usuario.points = req.body.points;
      usuario.availableTools = req.body.availableTools;
      usuario.availableColors = req.body.availableColors;
      usuario.save();
      res.status(200).json({ "Compra": true });

    }
  });
});

// Adiciona um usuario no banco de dados
router.post('/create-user', function(req, res) {
	var newUser = new User();
	newUser.username = req.body.username;
	newUser.email = req.body.email;
	newUser.password = req.body.password;
	newUser.color = req.body.color;
	// Adiciona cores padrão
	newUser.availableColors = [ {"id": 1, "name": "color-yellow", "desc": "Amarelo", "details": "#FFFF00"},
  								{"id": 2, "name": "color-green", "desc": "Verde", "details": "#32CD32"},
  								{"id": 3, "name": "color-blue", "desc": "Azul", "details": "#1E90FF"},
  								{"id": 4, "name": "color-black", "desc": "Preto", "details": "#000000"},
  								{"id": 5, "name": "color-orange", "desc": "Laranja", "details": "#FF9900"},
  								{"id": 6, "name": "color-red", "desc": "Vermelho", "details": "#FF0000"} ];
  	// Adiciona ferramentas padrão
	newUser.availableTools = [ {"id": 1, "name": "paint-brush-1", "desc": "Pincel 1", "details": 5} ];

	newUser.save( function(err) {
	  	if (err) {
	  		if (err.code == 11000) {
		  		res.status(400).json({error: 'Nome de usuario ou e-mail ja cadastrados'});
	  		} else {
	  			res.status(500).json({error: 'Ocorreu um erro ao salvar o usuario no banco de dados. Tente novamente mais tarde.'});
	  		}
	  	} else {
	  		res.status(201).json({message: 'Usuario salvo com sucesso'});
	  	}
	});
});

module.exports = router;
