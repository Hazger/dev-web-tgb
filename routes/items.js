 
var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser')
var router = express.Router();
router.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 
router.use(cookieParser())

var Tool = require('../models/tool').Tool;
var Color = require('../models/color').Color;

// Lista todos os usuarios
router.get('/tools', function(req, res) {
	if (req.cookies && req.cookies.loggedUser) {
		Color.find({}, {'_id': 0, '__v': 0}, function(err, docs) {
			availableColors = docs;
		    if (!err) {
		    	Tool.find({}, { '_id': 0 }, function(err, docs) {
		    		availableTools = docs;
		    		if (!err) {
		    			res.json(200, { 'availableColors': availableColors,  'availableTools': availableTools });
		    		} else {
				    	res.json(500, { message: 'Nao foi possivel carregar as ferramentas' });
				    }
		    	});
		    } else {
		    	res.json(500, { message: 'Nao foi possivel carregar as ferramentas' });
		    }
		});
	} else {
		res.json(400, { message: 'Requisicao nao autorizada' });
	}
});

module.exports = router;