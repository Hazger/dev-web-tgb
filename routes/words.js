var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser')
var router = express.Router();
router.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 
router.use(cookieParser())

var Word = require('../models/word').Word;

// Lista todos os usuarios
router.get('/word-options', function(req, res) {
	if (req.cookies && req.cookies.loggedUser) {
		var words = new Array();
		Word.findOneRandom({'level': 'Fácil'}, {'_id': 0, '__v': 0}, function(err, doc) {
		    if (!err) {
		    	words.push(doc);
		   		Word.findOneRandom({'level': 'Médio'}, {'_id': 0, '__v': 0}, function(err, doc) {
				    if (!err) {
				    	words.push(doc);
				   		Word.findOneRandom({'level': 'Difícil'}, {'_id': 0, '__v': 0}, function(err, doc) {
						    if (!err) {
						    	words.push(doc);
						    	res.json(200, { wordOptions: words });
						    } else {
						    	res.json(500, { message: 'Nao foi possivel carregar as ferramentas' });
						    }
						}); 	
				    } else {
				    	res.json(500, { message: 'Nao foi possivel carregar as ferramentas' });
				    }
				});
		    } else {
		    	res.json(500, { message: 'Nao foi possivel carregar as ferramentas' });
		    }
		});
	} else {
		res.json(400, { message: 'Requisicao nao autorizada' });
	}
});

module.exports = router;