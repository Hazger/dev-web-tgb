var express = require('express');
var cookieParser = require('cookie-parser');
var router = express.Router();
router.use(cookieParser());

//necessario pra rodar o auto deploy
var exec = require('child_process').exec;

/* GET home page. */
router.get('/', function(req, res, next) {
  if (req.cookies && req.cookies.loggedUser) {
      next();
  } else {
    res.redirect("/login.html");
  }
});

router.get('/index.html', function(req, res, next) {
  if (req.cookies && req.cookies.loggedUser) {
      next();
  } else {
    res.redirect("/login.html");
  }
});

router.get('/navigation-bar.html', function(req, res, next) {
  if (req.cookies && req.cookies.loggedUser) {
      next();
  } else {
    res.send('<!-- Nao permitido -->');
  }
});

//rota para dar auto deploy com base no git
router.all('/deploy', function(req, res) {
  exec('./deploy.sh',
    function (error, stdout, stderr) {
      if (error !== null) {
        //console.log('exec error: ' + error);
        res.send('Erro: ' + error);
      }else {
        res.send('Sucesso: ' + stdout);
      }
  });
});

module.exports = router;
