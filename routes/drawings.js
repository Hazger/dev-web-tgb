var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser')
var router = express.Router();
router.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 
router.use(cookieParser())

var Drawing = require('../models/drawing').Drawing;
var User = require('../models/user').User;
var Word = require('../models/word').Word;

// Lista os desenhos enviados para o usuário
router.get('/drawings-to-guess', function(req, res) {
	if (req.cookies && req.cookies.loggedUser) {
		Drawing.find({"toUser" : req.cookies.loggedUser, "guessed" : false}, {"toUser" : 0}, function(err, docs) {
			for (var i = 0; i < docs.length; i++) {
				docs[i].word = null;
			}
			if (!err) {
		    	res.status(200).json({ guess: docs });
		    } else {
		    	res.status(500).json({ message: 'Nao foi possivel carregar os desenhos a serem adivinhados' });
		    }
		});
	} else {
		res.status(400).json({ message: 'Requisicao nao autorizada' });
	}
});

// Armazena um desenho enviado para o usuário
router.post('/send-drawing-to-user', function(req, res) {
	if (req.cookies && req.cookies.loggedUser && req.body.theme) {
		Word.findOne({"word" : req.body.theme}, function(err, doc){
			if (!err && doc) {
				// Palavra encontrada, vamos salvar o desenho
				var drawingToSave = new Drawing();
				drawingToSave.fromUser = req.cookies.loggedUser;
				drawingToSave.toUser = req.body.toUser;
				drawingToSave.word = req.body.theme;
				drawingToSave.letters = req.body.theme.replace(/[^ -]/gi, '_');
				drawingToSave.drawing = req.body.userDrawing;
				drawingToSave.points = doc.points;
				drawingToSave.guessed = false;

				drawingToSave.save(function(err) {
					if (!err) {
				    	res.status(200).json({ "drawingSent": true });
				    } else {
				    	console.log(err);
				    	res.status(500).json({ message: 'Nao foi possivel enviar o desenho para o usuario selecionado' });
				    }
				});
			} else {
				res.status(400).json({ message: 'Palavra referente ao desenho nao encontrada' });
			}
		});
		
	} else {
		res.status(400).json({ message: 'Requisicao nao autorizada' });
	}
});

// Recebe uma requisição para adivinhar um desenho
router.get('/guess', function(req, res) {
	if (req.cookies && req.cookies.loggedUser) {
		var drawingId = req.param("id");
		var drawingGuess = req.param("guess");
		Drawing.findOne({"_id" : drawingId, "word" : drawingGuess.toLowerCase()}, function(err, drawing) {
			if (!err) {
				if (drawing) {
					// Atualiza a pontuacao do usuario que fez o desenho
					User.findOne({"username" : drawing.fromUser}, function(err, fromUser){
						fromUser.points += drawing.points;
						fromUser.save(function(err) {
							if (!err) {
								// Atualiza a pontuacao do usuario que adivinhou o desenho
								User.findOne({"username" : drawing.toUser}, function(err, toUser){
									toUser.points += drawing.points;
									toUser.save(function(err) {
										if (!err) {
											// Atualiza o desenho para 'adivinhado'
											drawing.guessed = true;
											drawing.save(function(err) {
												if (!err) {
													res.status(200).json({ "guessed": true });
												} else {
													res.status(500).json({ message: 'Nao foi possivel atualizar o desenho adivinhado' });		
												}
											});
										} else {
											res.status(500).json({ message: 'Nao foi possivel atualizar a pontuacao dos usuarios' });
										}
									});
								});
							} else {
								res.status(500).json({ message: 'Nao foi possivel atualizar a pontuacao dos usuarios' });
							}
						});
					});
			    } else {
					res.status(200).json({ "guessed": false });
			    }
		    } else {
		    	res.status(500).json({ message: 'Nao foi possivel verificar se o desenho foi identificado incorretamente' });
		    }
		});
	} else {
		res.status(400).json({ message: 'Requisicao nao autorizada' });
	}
});

module.exports = router;