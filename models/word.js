var mongoose = require('mongoose')
var random = require('mongoose-simple-random');
var Schema = mongoose.Schema;

var wordSchema = new Schema({ 
	word : {
		type: String,
		required: true
	}, 
	level : {
		type: String,
		required: true
	}, 
	color : {
		type: String,
		required: true
	},
	points : {
		type: Number,
		required: true
	}
});
      
wordSchema.plugin(random);

var word = mongoose.model('word', wordSchema);
      
module.exports = {
  Word: word
};