var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var itemDetailsSchema = new Schema({ 
	id : {
		type: Number,
		required: true
	},
	name : {
		type: String,
		required: true
	}, 
	desc : {
		type: String,
		required: true
	}, 
	details : {
		type: String,
		required: true
	}
});
      
var userSchema = new Schema({
    username : { 
    	type: String, 
    	required: true, 
    	trim: true, 
    	index: { unique: true } 
    },
    password : { 
    	type: String, 
    	required: true
    },
  	points : { 
  		type: Number,
  		default: 0
  	},
  	email : { 
  		type: String, 
  		required: true
  	},
  	color : { 
  		type: String, 
  		required: true
  	},
  	availableColors : [ itemDetailsSchema ],
  	availableTools : [ itemDetailsSchema ]
});
      
var user = mongoose.model('user', userSchema);
      
module.exports = {
  User: user
};