var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var colorSchema = new Schema({ 
	id : {
		type: Number,
		required: true
	},
	name : {
		type: String,
		required: true
	}, 
	desc : {
		type: String,
		required: true
	}, 
	details : {
		type: String,
		required: true
	},
	price : {
		type: Number,
		required: true
	}
});
      
var color = mongoose.model('color', colorSchema);
      
module.exports = {
  Color: color
};