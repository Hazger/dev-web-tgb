var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var drawingSchema = new Schema({ 
	fromUser : {
		type: String,
		required: true
	}, 
	toUser : {
		type: String,
		required: true
	},
	points : {
		type: Number,
		required: true
	}, 
	word : {
		type: String,
		required: true
	}, 
	drawing : {
		type: String,
		required: true
	}, 
	letters : {
		type: String,
		required: true
	}, 
	guessed : {
		type: Boolean,
		default: false
	}
});
      
var drawing = mongoose.model('drawing', drawingSchema);
      
module.exports = {
  Drawing: drawing
};