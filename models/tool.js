var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var toolSchema = new Schema({ 
	id : {
		type: Number,
		required: true
	},
	name : {
		type: String,
		required: true
	}, 
	desc : {
		type: String,
		required: true
	}, 
	details : {
		type: String,
		required: true
	},
	price : {
		type: Number,
		required: true
	}
});
      
var tool = mongoose.model('tool', toolSchema);
      
module.exports = {
  Tool: tool
};